package com.springsample.pkg.Services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springsample.pkg.Models.DB.Project;
import com.springsample.pkg.Repositories.ProjectsRepository;

@Service
public class ProjectService implements IProjectService{

	@Autowired
	private ProjectsRepository projectsRepository;
	
	@Override
	public Project findById(int id) {
		// TODO Auto-generated method stub
		return projectsRepository.findById(id);
	}

	@Override
	public void save(Project project) {
		// TODO Auto-generated method stub
		projectsRepository.save(project);
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		projectsRepository.deleteByProjectId(id);
		
	}

	@Override
	public List<Project> findAll() {
		// TODO Auto-generated method stub
		return  projectsRepository.findAll();
	}

}
