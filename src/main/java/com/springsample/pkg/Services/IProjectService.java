package com.springsample.pkg.Services;

import java.util.List;
import java.util.Set;

import com.springsample.pkg.Models.DB.Project;

public interface IProjectService {
	
	Project findById(int id);
	
	void save(Project project);
	
	void deleteById(int id);
	
	List<Project> findAll();

}
