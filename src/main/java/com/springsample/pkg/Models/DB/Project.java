package com.springsample.pkg.Models.DB;

import java.util.List;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity

@Table(name = "projects")
public class Project {
	
	
	
	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name = "project_id")
    private int id;
	

    @Column(name = "name")
	private String name ;
    
    @Column(name = "notes")
	private String notes ;
	   
	  

	  
	
	

}
