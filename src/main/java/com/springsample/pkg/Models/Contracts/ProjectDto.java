package com.springsample.pkg.Models.Contracts;

import java.util.List;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor


public class ProjectDto {
	
	
    private int id;
	
	@NotNull(message="name can not be empty")  
	@NotEmpty(message="name can not be empty") 
	private String name ;
    
	private String notes ;
	   
	  

	  
	
	

}
