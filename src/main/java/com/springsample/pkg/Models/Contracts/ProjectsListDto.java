package com.springsample.pkg.Models.Contracts;

import java.util.List;

import com.springsample.pkg.Models.DB.Project;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectsListDto {
	
	private List<Project> projectsList; 
	
	

}
