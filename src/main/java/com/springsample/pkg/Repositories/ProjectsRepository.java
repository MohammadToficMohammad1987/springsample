package com.springsample.pkg.Repositories;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springsample.pkg.Models.DB.Project;



@Repository("projectsRepository")
public interface ProjectsRepository extends JpaRepository<Project, Long> {
  
    Project findById(int id);
    boolean existsById(Integer id);

	
	
	 @Modifying
	  @Transactional
	  @Query(value="delete from projects  where project_id = ?1",nativeQuery = true)
	  void deleteByProjectId(int id);
  
   
    
    

    
}
