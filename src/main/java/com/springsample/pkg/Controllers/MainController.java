package com.springsample.pkg.Controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.springsample.pkg.Models.Contracts.ProjectDto;
import com.springsample.pkg.Models.Contracts.ProjectsListDto;
import com.springsample.pkg.Models.DB.Project;
import com.springsample.pkg.Services.ProjectService;


@Controller
public class MainController {
	
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value={"/"}, method = RequestMethod.GET)
    public String  Index(Model model){
		
	
        return "Index";
    }
	

	@RequestMapping(value={"/projects"}, method = RequestMethod.GET)
	@ResponseBody
    public ProjectsListDto  GetProjects(){

		return ProjectsListDto.builder().projectsList(projectService.findAll()).build();
    }
	
	@RequestMapping(value={"/projects"}, method = RequestMethod.POST)
	@ResponseBody
    public void  PostProject(@Valid @RequestBody ProjectDto projectDto){
		
		Project project=Project.builder()
				.name(projectDto.getName())
				.notes(projectDto.getNotes())
				.build();
		projectService.save(project);
		
    }
	
	//note this would override response
	//@ResponseStatus(value = HttpStatus.NO_CONTENT) 
	
	@RequestMapping(value={"/projects/{id}"}, method = RequestMethod.GET)
	@ResponseBody
    public ProjectDto  GetProjectById(@PathVariable int id){
		
		Project project=projectService.findById(id);
		return ProjectDto.builder()
				.id(project.getId())
				.name(project.getName())
				.notes(project.getNotes())
				.build();
    }
	
	@RequestMapping(value={"/projects/{id}"}, method = RequestMethod.DELETE)
	@ResponseBody
    public void  DeleteProjectById(@PathVariable int id){
		
		 projectService.deleteById(id);
    }
	
	
	@RequestMapping(value={"/CheckValidation"}, method = RequestMethod.GET)
    public String  CheckValidation(Model model){
		
		ProjectDto p1=new ProjectDto();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<ProjectDto>> violations = validator.validate(p1);
		List<String> violationStrings=new ArrayList<String>();
		for (ConstraintViolation<ProjectDto> violation : violations)
		{
			violationStrings.add(violation.getMessage());
	    }
		//List<String> supplierNames = Arrays.asList("sup1", "sup2", "sup3");
		model.addAttribute("violations", violationStrings);
        return "Index";
    }
	
	
	
}
