package com.springsample.pkg;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.springsample.pkg.Models.DB.Project;

@SpringBootTest
public class RunTest1 extends ITestWithMockConfig1{
	
	@Test
	void Test1() {
		Assert.isTrue(_projectsRepository.findAll().size()==1,"Test 1");
	}
	
	@Test
	void Test2() {
		Project p1=new Project(1,"lalo","lolo");
		List<Project> list=new ArrayList<Project> ();
		list.add(p1);
		when(service.findAll()).thenReturn(list);
		Assert.isTrue(_MainController.GetProjects().getProjectsList().get(0).getName().equals("lalo"),"Test 2");
	}

}
