package com.springsample.pkg;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import com.springsample.pkg.Controllers.MainController;
import com.springsample.pkg.Models.DB.Project;
import com.springsample.pkg.Repositories.ProjectsRepository;
import com.springsample.pkg.Services.ProjectService;

@Transactional // make it reset DB every test
class ITestWithRealDI {

	  @Resource
	  protected ProjectsRepository _projectsRepository;
	  
	  @Resource
	  protected MainController _MainController;
	  

}

     
   