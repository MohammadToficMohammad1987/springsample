package com.springsample.pkg;



import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.springsample.pkg.Models.DB.Project;

@SpringBootTest
public class RunTest2 extends ITestWithRealDI{
	
	@Test
	void Test1() {
		//Arrange
		 Project p1=new Project(0,"lolo","lolo");
		 
		 //Act
		_projectsRepository.save(p1);
		
		//Check
		Assert.isTrue(_projectsRepository.findAll().size()==1,"Test 1");
	}
	
	@Test
	void Test2() {
		//Arrange
		 Project p1=new Project(1,"lala","lolo");
		 
		 //Act
		_projectsRepository.save(p1);
		
		//Check
		Assert.isTrue(_MainController.GetProjects().getProjectsList().get(0).getName().equals("lala"),"Test 2");
	}
	
	@Test
	void Test3() {
		//Arrange
		 Project p1=new Project(1,"lala","lolo");
		 
		 //Act
		_projectsRepository.save(p1);
		
		//Check
		Assert.isTrue(_MainController.GetProjects().getProjectsList().get(0).getName().equals("lala"),"Test 3");
	}

}
