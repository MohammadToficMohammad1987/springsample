package com.springsample.pkg;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;

import com.springsample.pkg.Controllers.MainController;
import com.springsample.pkg.Models.DB.Project;
import com.springsample.pkg.Repositories.ProjectsRepository;
import com.springsample.pkg.Services.ProjectService;

@Transactional // make it reset DB every test
@AutoConfigureMockMvc
//@WebMvcTest
class ITestWithMockConfig1 {

	  @Resource
	protected ProjectsRepository _projectsRepository;
	  
	  @Resource
	  protected MainController _MainController;
	  
	  @Autowired
	  protected MockMvc mockMvc;
	  
	  @MockBean
	  protected  ProjectService service;
	  
	  @BeforeEach
	  protected  void prepare() {
		  Project p1=new Project(0,"lolo","lolo");
		 _projectsRepository.save(p1); 
		  
	  }
	  
	
	
	
	

}

     
   